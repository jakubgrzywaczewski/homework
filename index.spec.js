const { 
    countCoins,
    inputData,
    packCoins,
    packSizes
} = require('./index');

describe('countCoins()', () => {
    it('should return correct values with given data', () => {
        const dummyInput = [1, 2, 3, 3, 3];
        const result = countCoins(dummyInput);
        const expectedResult = {
            '1': 1,
            '2': 1,
            '3': 3,
        };

        expect(result).toStrictEqual(expectedResult);
    });

    it('should return empty object with empty input', () => {
        const dummyInput = [];
        const result = countCoins(dummyInput);
        const expectedResult = {};

        expect(result).toStrictEqual(expectedResult);
    });
});

describe('packCoins()', () => {
    it('should return correct values with given data', () => {
        const dummyInput = [1, 2, 2, 2, 2, 5, 5, 5, 5, 5];
        const packSizes = {
            '1': 2,
            '2': 3,
            '5': 2,
        }
        const result = packCoins(dummyInput, packSizes);
        const expectedResult = {
            '1': [0, 1],
            '2': [1, 1],
            '5': [2, 1],
        };

        expect(result).toStrictEqual(expectedResult);
    });

    it('should return empty object with empty input', () => {
        const result = packCoins();
        const expectedResult = {};

        expect(result).toStrictEqual(expectedResult);
    });

    it('should return correct values with given initial data', () => {
        const result = packCoins(inputData, packSizes);
        const expectedResult = {
            '1': [40, 34],
            '2': [40, 10],
            '5': [55, 13],
            '10': [33, 40],
            '20': [85, 11],
            '50': [42, 12],
        };

        expect(result).toStrictEqual(expectedResult);
    });
});